@extends('layout.master')

@section('judul')
<h1>SELAMAT DATANG! {{$name}} {{$nama}}</h1>
@endsection

@section('content')
    <h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
@endsection