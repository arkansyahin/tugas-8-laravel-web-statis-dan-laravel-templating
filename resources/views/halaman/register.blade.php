@extends('layout.master')

@section('judul')
<h2>Buat Account Baru</h2>
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
	<label>First Name:</label><br>
    <input type="text" name="name"><br><br>
    <label>Last Name:</label><br>
    <input type="text" name="nama"><br><br>
    <label>Gender</label><br><br>
    <input type="radio" name="wn">Male<br>
    <input type="radio" name="wn">Female<br><br>
    <label>Nationality</label><br><br>
    <select name="nationality">
    	<option value="indonesia">Indonesia</option>
    	<option value="malaysia">Malaysia</option>
        <option value="thailand">Thailand</option>
	</select><br><br>
    <label>Language Spoken</label><br><br>
    <input type="checkbox" name="languange">Bahasa Indonesia<br>
    <input type="checkbox" name="languange">English<br>
    <input type="checkbox" name="languange">Other<br><br>
    <label>Bio</label><br><br>
    <textarea name="pesan" rows="10" cols="30"></textarea><br>
    <input type="submit" value="Sign UP"><br><br>
@endsection