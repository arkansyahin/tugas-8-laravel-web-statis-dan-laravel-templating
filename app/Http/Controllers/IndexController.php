<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view('halaman.index');
    }
    public function table()
    {
        return view('table.data-tables');
    }
}
